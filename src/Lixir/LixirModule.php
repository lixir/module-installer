<?php

namespace Lixir\Composer;

use Composer\Composer;
use Composer\IO\IOInterface;
use Composer\Plugin\PluginInterface;

class LixirModule implements PluginInterface
{
    public function activate(Composer $composer, IOInterface $io)
    {
        $lixirInstaller = new LixirInstaller($io, $composer);
        $composer->getInstallationManager()->addInstaller($lixirInstaller);
    }
}
