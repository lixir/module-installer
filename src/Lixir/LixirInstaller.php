<?php

namespace Lixir\Composer;

use Composer\Installer\LibraryInstaller;
use Composer\Package\PackageInterface;

class LixirInstaller extends LibraryInstaller
{
    public $packages = [
        "lixir-module",
        "lixir-theme"
    ];

    public function getInstallPath(PackageInterface $package)
    {
        $type = $package->getType();
        $names = $package->getNames();

        if (is_array($names)) {
            $names = $names[0];
        }

        if ("lixir-module" == $type) {
            list($vendor, $package) = explode("/", $names);

            return "modules/" . $vendor . "/" . $package;
        }

        if ("lixir-theme" == $type) {
            list($vendor, $package) = explode("/", $names);

            return "themes/" . $vendor . "/" . $package;
        }
    }

    public function supports($packageType)
    {
        return in_array($packageType, $this->packages);
    }
}
